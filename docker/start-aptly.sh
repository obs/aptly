#!/bin/bash

set -e

shopt -s nullglob
for key in /aptly/gpg/*.asc; do
  echo "NOTE: Importing gpg key: $key"
  gpg --import $key
done

profargs=()
if [ -n "$APTLY_PROFILE" ]; then
  profdir=/aptly/data/profile/$(date -Isec)
  mkdir -p $profdir
  profargs=(-cpuprofile=$profdir/cpu.prof -memprofile=$profdir/mem.prof)
fi

set -x
exec aptly "${profargs[@]}" api serve "$@"
